﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CITest
{
    public class TestSum
    {
        public static double Sum(double firstNumber, double secondNumber)
        {
            return firstNumber + secondNumber;
        }
    }
}
